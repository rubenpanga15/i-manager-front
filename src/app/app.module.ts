import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { ErrorDialogComponent } from './dialogs/error-dialog/error-dialog.component';
import { InfoDialogComponent } from './dialogs/info-dialog/info-dialog.component';
import { SuccessDialogComponent } from './dialogs/success-dialog/success-dialog.component';
import { WaitingDialogComponent } from './dialogs/waiting-dialog/waiting-dialog.component';
import { SharedModule } from './modules/shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MainCommandeComponent } from './components/main-commande/main-commande.component';
import { TableDialogComponent } from './dialogs/table-dialog/table-dialog.component';
import { ApartComponent } from './components/apart/apart.component';
import { CreerCommandeDialogComponent } from './dialogs/creer-commande-dialog/creer-commande-dialog.component';
import { AjouterArticleDialogComponent } from './dialogs/ajouter-article-dialog/ajouter-article-dialog.component';
import { SelectArticleDialogComponent } from './dialogs/select-article-dialog/select-article-dialog.component';
import { SelectServeurDialogComponent } from './dialogs/select-serveur-dialog/select-serveur-dialog.component';
import { MainGestionStockComponent } from './components/main-gestion-stock/main-gestion-stock.component';
import { MainPaiementComponent } from './components/main-paiement/main-paiement.component';
import { MainConfigurationComponent } from './components/main-configuration/main-configuration.component';
import { DashboardSettingsComponent } from './components/dashboard-settings/dashboard-settings.component';
import { EditCategorieArticlesComponent } from './components/edit-categorie-articles/edit-categorie-articles.component';
import { EditDeviseComponent } from './components/edit-devise/edit-devise.component';
import { EditSiteComponent } from './components/edit-site/edit-site.component';
import { EditFournisseurComponent } from './components/edit-fournisseur/edit-fournisseur.component';
import { EditTableComponent } from './components/edit-table/edit-table.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { EditArticleComponent } from './components/edit-article/edit-article.component';
import { EditCommandeComponent } from './components/edit-commande/edit-commande.component';
import { EditTauxChangeComponent } from './components/edit-taux-change/edit-taux-change.component';
import { EditConfigTarifComponent } from './components/edit-config-tarif/edit-config-tarif.component';
import { EditTaxeComponent } from './components/edit-taxe/edit-taxe.component';
import { ViewCommandeDialogComponent } from './dialogs/view-commande-dialog/view-commande-dialog.component';
import { EditFactureComponent } from './components/edit-facture/edit-facture.component';
import { AjouterCommandeComponent } from './dialogs/ajouter-commande/ajouter-commande.component';
import { ViewFactureDialogComponent } from './dialogs/view-facture-dialog/view-facture-dialog.component';
import { MainFactureComponent } from './components/main-facture/main-facture.component';
import { ListeFacturesComponent } from './components/liste-factures/liste-factures.component';
import { ListeCommandesComponent } from './components/liste-commandes/liste-commandes.component';
import { SelectFactureDialogComponent } from './dialogs/select-facture-dialog/select-facture-dialog.component';
import { EditPaiementComponent } from './components/edit-paiement/edit-paiement.component';
import { ListeApurementComponent } from './components/liste-apurement/liste-apurement.component';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmDialogComponent,
    ErrorDialogComponent,
    InfoDialogComponent,
    SuccessDialogComponent,
    WaitingDialogComponent,
    LoginComponent,
    MainComponent,
    HeaderComponent,
    DashboardComponent,
    MainCommandeComponent,
    TableDialogComponent,
    ApartComponent,
    CreerCommandeDialogComponent,
    AjouterArticleDialogComponent,
    SelectArticleDialogComponent,
    SelectServeurDialogComponent,
    MainGestionStockComponent,
    MainPaiementComponent,
    MainConfigurationComponent,
    DashboardSettingsComponent,
    EditCategorieArticlesComponent,
    EditDeviseComponent,
    EditSiteComponent,
    EditFournisseurComponent,
    EditTableComponent,
    EditUserComponent,
    EditArticleComponent,
    EditCommandeComponent,
    EditTauxChangeComponent,
    EditConfigTarifComponent,
    EditTaxeComponent,
    ViewCommandeDialogComponent,
    EditFactureComponent,
    AjouterCommandeComponent,
    ViewFactureDialogComponent,
    MainFactureComponent,
    ListeFacturesComponent,
    ListeCommandesComponent,
    SelectFactureDialogComponent,
    EditPaiementComponent,
    ListeApurementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
