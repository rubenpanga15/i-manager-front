import { CategorieArticle } from "./categorie-article";
import { Tarif } from './tarif';

export class Article {
    id: number;
    nom: string;
    description: string;
    code: string;
    fkCategorieArticle: number;
    uniteMesure: string;
    stockRestant: number;
    stockAlert: number;
    remise: number;
    remiseApartirDe: number;
    isRemisePourcentage: boolean;
    fkSite: number;
    status: boolean;
    dateCreat: Date;

    categorieArticle: CategorieArticle;
    tarif: Tarif;
}
