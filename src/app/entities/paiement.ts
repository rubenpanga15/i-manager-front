import { Facture } from "./facture";
import { Site } from "./site";
import { User } from "./user";

export class Paiement {
    id: number;
    fkFacture: string;
    client: string;
    montantApayer: number;
    montantPaye: number;
    solde: number;
    fkDevise: string;
    fkUser: number;
    fkSite: number;
    datePaiement: Date;
    status: boolean;
    dateCreat: Date

    facture: Facture;
    user: User;
    site: Site
}
