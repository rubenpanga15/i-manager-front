export class Fournisseur {
    id: number;
    noms: string;
    telephone: string;
    email: string;
    fkSite: number;
    status: boolean;
    dateCreat: Date
}
