import { Fournisseur } from "./fournisseur";
import { Site } from "./site";
import { User } from "./user";

export class SortieStock {
    numero: string;
    dateEntree: Date;
    motif: string
    fkUser: number;
    fkSite: number;
    status: boolean;
    dateCreat: Date;

    fournisseur: Fournisseur;
    user: User;
    site: Site;
}
