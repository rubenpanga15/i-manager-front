import { Article } from "./article";

export class DetailEntreeStock {
    id: number;
    fkArticle: number;
    fkEntreeStock: string;
    dateExpiration: Date;
    numeroSerie: string;
    volume: number;
    valeur: number;
    prixUnitaire: number;
    montantTotal: number;
    isLot: boolean;
    status: boolean;
    dateCreat: Date

    article: Article
}
