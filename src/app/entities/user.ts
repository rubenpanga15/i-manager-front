export class User {
    id: number;
    username: string;
    password: string;
    noms: string;
    sexe: string;
    telephone: string;
    email: string;
    adresse: string;
    role: string;
    fkSite: number;
    status: string;
    dateCreat: Date;
}
