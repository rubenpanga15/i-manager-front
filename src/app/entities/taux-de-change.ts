import { Devise } from './devise';

export class TauxDeChange {
    id: number;
    fkDeviseFrom: string;
    fkDeviseTo: string;
    fkSite: number;
    taux: number;
    status: boolean;
    dateCreat: Date

    from: Devise;
    to: Devise;
}
