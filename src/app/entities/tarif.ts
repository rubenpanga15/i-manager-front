import { Devise } from '../entities/devise';

export class Tarif {
    id: number;
    montant: number;
    fkDevise: string;
    status: boolean;
    dateCreat: Date;
    fkSite: number;
    fkArticle: number;
    
    devise: Devise
}
