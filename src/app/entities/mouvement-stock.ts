import { Article } from "./article";

export class MouvementStock {
    id: number;
    fkArticle: number;
    sens: string;
    volume: number;
    valeur: number;
    dateOperation: Date;
    fkSite: number;
    status: boolean;
    dateCreat: Date

    article: Article;
}
