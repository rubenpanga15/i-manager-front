import { DetailCommande } from "./detail-commande";
import { User } from "./user";

export class Commande {
    numero: string;
    fkUser: number;
    table: string;
    client: string;
    fkClient: string;
    fkSite: number;
    fkFacture: string;
    etat: string;
    status: boolean;
    dateCreat: Date;
    fkDefaultDevise: string;
    montantTotal: number;

    user: User
    details: DetailCommande[]
}

