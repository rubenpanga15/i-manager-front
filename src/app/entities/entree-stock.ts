import { Fournisseur } from "./fournisseur";
import { Site } from "./site";
import { User } from "./user";

export class EntreeStock {
    numero: string;
    dateEntree: Date;
    fkFournisseur: number;
    fkUser: number;
    fkSite: number;
    status: boolean;
    dateCreat: Date;

    fournisseur: Fournisseur;
    user: User;
    site: Site;
}
