import { Devise } from './devise';

export class Taxe {
    id: number;
    description: string;
    montant: number;
    fkDevise: string;
    isPourcentage: boolean;
    fkSite: number;
    status: boolean;
    dateCreat: Date

    devise: Devise;
}
