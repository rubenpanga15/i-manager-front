import { Article } from "./article";

export class DetailSortieStock {
    id: number;
    fkArticle: number;
    fkSortieStock: string;
    numeroSerie: string;
    volume: number;
    valeur: number;
    isLot: boolean;
    status: boolean;
    dateCreat: Date;

    article: Article
}
