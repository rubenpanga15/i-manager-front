import { Article } from "./article";

export class DetailCommande {
    id: number;
    fkCommande: string;
    fkArticle: number;
    volume: number;
    prixUnitaire: number;
    montant: number;
    isAccompagnement: boolean;
    status: boolean;
    dateCreat: Date;

    article: Article
}
