export class ConfigFacture {
    id: number;
    remise: number;
    remiseApartirDe: number;
    isPourcentage: boolean;
    applyTaxe: boolean;
    fkSite: number;
    status: number;
    dateCreat: Date;
}
