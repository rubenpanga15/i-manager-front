import { Commande } from "./commande";
import { DetailFacture } from "./detail-facture";
import { Site } from "./site";
import { User } from "./user";

export class Facture {
    numero: string;
    fkUser: number;
    table: string;
    fkTable: number;
    fkClient: number;
    client: string;
    montantTotal: number;
    fkDefaultDevise: string;
    typeFacture: string;
    fkSite: number;
    etat: string;
    status: boolean;
    dateCreat: Date;
    isRemisePourcentage: boolean;
    remise:number;
    applyTva: boolean;
    tva: number;
    user: User;
    site: Site;
    tauxDeChange: number;

    commandes: Commande[];
    details: DetailFacture[]
}
