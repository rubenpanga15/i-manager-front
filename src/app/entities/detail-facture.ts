import { Article } from "./article";

export class DetailFacture {
    id: number;
    fkFacture: string;
    volume: number;
    prixUnitaire: number;
    montant: number;
    isAccompagnement: boolean;
    status: boolean;
    dateCreat: Date;
    fkArticle: number;

    article: Article
}
