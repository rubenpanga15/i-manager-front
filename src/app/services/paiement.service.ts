import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Paiement } from '../entities/paiement';
import { AppConst } from '../utilities/app-const';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpUrls } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';
import { PrinterService } from './printer-service.service';

@Injectable({
  providedIn: 'root'
})
export class PaiementService {

  constructor(
    private appConfig: AppConfigService,
    private httpClient: HttpClient,
    private printerService: PrinterService
  ) { }

  public effectuerUnPaiement(data: string): Promise<HttpDataResponse<number>>{
    return new Promise<HttpDataResponse<number>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EFFECTUER_PAIEMENT, data)
      .subscribe(
        (result: HttpDataResponse<number>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListePaiements(data: string): Promise<HttpDataResponse<Paiement[]>>{
    return new Promise<HttpDataResponse<Paiement[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation de la liste des paiements....');
      this.httpClient.post(HttpUrls.URL_LISTE_PAIEMENT, data)
      .subscribe(
        (result: HttpDataResponse<Paiement[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getTodayListePaiements(): Promise<HttpDataResponse<Paiement[]>>{
    return new Promise<HttpDataResponse<Paiement[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation de la liste des paiements....');
      this.httpClient.get(HttpUrls.URL_LISTE_TODAY_PAIEMENT)
      .subscribe(
        (result: HttpDataResponse<Paiement[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
