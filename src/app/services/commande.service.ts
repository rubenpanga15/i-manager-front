import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Commande } from '../entities/commande';
import { Facture } from '../entities/facture';
import { AppConst } from '../utilities/app-const';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpUrls } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private httpClient: HttpClient
  ) { }

  public saveCommande(data: string): Promise<HttpDataResponse<string>>{
    return new Promise<HttpDataResponse<string>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Enregistrement de la commande en cours...');
      this.httpClient.post(HttpUrls.URL_SAVE_COMMANDE, data)
      .subscribe(
        (result: HttpDataResponse<string> ) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public saveFacture(data: string): Promise<HttpDataResponse<string>>{
    return new Promise<HttpDataResponse<string>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Enregistrement de la facture en cours...');
      this.httpClient.post(HttpUrls.URL_SAVE_FACTURE, data)
      .subscribe(
        (result: HttpDataResponse<string> ) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeCommandeEnAttente(): Promise<HttpDataResponse<Commande[]>>{
    return new Promise<HttpDataResponse<Commande[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Recuperation des commandes en attente...');
      this.httpClient.get(HttpUrls.URL_GET_COMMANDE_ATTENTE)
      .subscribe(
        (result: HttpDataResponse<Commande[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeFactures(data: string): Promise<HttpDataResponse<Facture[]>>{
    return new Promise<HttpDataResponse<Facture[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation des factures en cours...');
      this.httpClient.post(HttpUrls.URL_LISTE_FACTURES, data)
      .subscribe(
        (result: HttpDataResponse<Facture[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeFacturesToday(): Promise<HttpDataResponse<Facture[]>>{
    return new Promise<HttpDataResponse<Facture[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation des factures en cours...');
      this.httpClient.post(HttpUrls.URL_LISTE_TODAY_FACTURES, '')
      .subscribe(
        (result: HttpDataResponse<Facture[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeCommandes(data: string): Promise<HttpDataResponse<Commande[]>>{
    return new Promise<HttpDataResponse<Commande[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation des commandes en cours...');
      this.httpClient.post(HttpUrls.URL_LISTE_COMMANDES, data)
      .subscribe(
        (result: HttpDataResponse<Commande[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeFacturesParEtat(data: string): Promise<HttpDataResponse<Facture[]>>{
    return new Promise<HttpDataResponse<Facture[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting('Récuperation des factures en cours...');
      this.httpClient.post(HttpUrls.URL_LISTE_FACTURE_ETAT, data)
      .subscribe(
        (result: HttpDataResponse<Facture[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode == 'KO')
            reject(result.error.errorDescription);
          resolve(result);
        },
        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }
}
