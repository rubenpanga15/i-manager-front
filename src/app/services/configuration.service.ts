import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpDataResponse } from '../utilities/http-data-response';
import { AppConfigService } from './app-config.service';
import { CategorieArticle } from '../entities/categorie-article';
import { Devise } from '../entities/devise';
import { HttpUrls } from '../utilities/http-urls';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';
import { Site } from '../entities/site';
import { Fournisseur } from '../entities/fournisseur';
import { User } from '../entities/user';
import { Article } from '../entities/article';
import { TauxDeChange } from '../entities/taux-de-change';
import { Taxe } from '../entities/taxe';
import { ConfigFacture } from '../entities/config-facture';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private httpClient: HttpClient
  ) { }

  public editCategorie(data: string): Promise<HttpDataResponse<CategorieArticle>>{
    return new Promise<HttpDataResponse<CategorieArticle>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_CATEGORIE_ARTICLE, data)
      .subscribe(
        (result: HttpDataResponse<CategorieArticle>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListCategorieArticle(): Promise<HttpDataResponse<CategorieArticle[]>>{
    return new Promise<HttpDataResponse<CategorieArticle[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_CATEGORIE_ARTICLE)
      .subscribe(
        (result: HttpDataResponse<CategorieArticle[]>) => {
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editDevise(data: string): Promise<HttpDataResponse<Devise>>{
    return new Promise<HttpDataResponse<Devise>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_DEVISE, data)
      .subscribe(
        (result: HttpDataResponse<Devise>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeDevise(): Promise<HttpDataResponse<Devise[]>>{
    return new Promise<HttpDataResponse<Devise[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_DEVISE)
      .subscribe(
        (result: HttpDataResponse<Devise[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getDefaultDevise(): Promise<HttpDataResponse<Devise>>{
    return new Promise<HttpDataResponse<Devise>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.get(HttpUrls.URL_GET_DEFAULT_DEVISE)
      .subscribe(
        (result: HttpDataResponse<Devise>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editSite(data: string): Promise<HttpDataResponse<Site>>{
    return new Promise<HttpDataResponse<Site>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_SITE, data)
      .subscribe(
        (result: HttpDataResponse<Site>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeSites(): Promise<HttpDataResponse<Site[]>>{
    return new Promise<HttpDataResponse<Site[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_SITE)
      .subscribe(
        (result: HttpDataResponse<Site[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editFournisseur(data: string): Promise<HttpDataResponse<Fournisseur>>{
    return new Promise<HttpDataResponse<Fournisseur>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_FOURNISSEUR, data)
      .subscribe(
        (result: HttpDataResponse<Fournisseur>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeFournisseurs(): Promise<HttpDataResponse<Fournisseur[]>>{
    return new Promise<HttpDataResponse<Fournisseur[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_FOURNISSEUR)
      .subscribe(
        (result: HttpDataResponse<Fournisseur[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editUser(data: string): Promise<HttpDataResponse<User>>{
    return new Promise<HttpDataResponse<User>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_USER, data)
      .subscribe(
        (result: HttpDataResponse<User>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeUsers(): Promise<HttpDataResponse<User[]>>{
    return new Promise<HttpDataResponse<User[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_USER)
      .subscribe(
        (result: HttpDataResponse<User[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editArticle(data: string): Promise<HttpDataResponse<Article>>{
    return new Promise<HttpDataResponse<Article>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.ULR_EDIT_ARTICLE, data)
      .subscribe(
        (result: HttpDataResponse<Article>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeArticles(): Promise<HttpDataResponse<Article[]>>{
    return new Promise<HttpDataResponse<Article[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.ULR_EDIT_ARTICLE)
      .subscribe(
        (result: HttpDataResponse<Article[]>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editTauxDeChange(data: string): Promise<HttpDataResponse<TauxDeChange>>{
    return new Promise<HttpDataResponse<TauxDeChange>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_TAUX_DECHANGE, data)
      .subscribe(
        (result: HttpDataResponse<TauxDeChange>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getTauxDeChange(data: string): Promise<HttpDataResponse<TauxDeChange>>{
    return new Promise<HttpDataResponse<TauxDeChange>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_GET_TAUX, data)
      .subscribe(
        (result: HttpDataResponse<TauxDeChange>) => {
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeTauxDeChange(): Promise<HttpDataResponse<TauxDeChange[]>>{
    return new Promise<HttpDataResponse<TauxDeChange[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_TAUX_DECHANGE)
      .subscribe(
        (result: HttpDataResponse<TauxDeChange[]>) => {
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editTaxe(data: string): Promise<HttpDataResponse<Taxe>>{
    return new Promise<HttpDataResponse<Taxe>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_TAXE, data)
      .subscribe(
        (result: HttpDataResponse<Taxe>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getListeTaxe(): Promise<HttpDataResponse<Taxe[]>>{
    return new Promise<HttpDataResponse<Taxe[]>>((resolve, reject) => {
      this.appConfig.onStartWaiting()
      this.httpClient.get(HttpUrls.URL_EDIT_TAXE)
      .subscribe(
        (result: HttpDataResponse<Taxe[]>) => {
          console.log(result)
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public editConfig(data: string): Promise<HttpDataResponse<ConfigFacture>>{
    return new Promise<HttpDataResponse<ConfigFacture>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_EDIT_CONFIG, data)
      .subscribe(
        (result: HttpDataResponse<ConfigFacture>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

  public getConfig(data: string): Promise<HttpDataResponse<ConfigFacture>>{
    return new Promise<HttpDataResponse<ConfigFacture>>((resolve, reject) => {
      this.appConfig.onStartWaiting();
      this.httpClient.post(HttpUrls.URL_GET_CONFIG, data)
      .subscribe(
        (result: HttpDataResponse<ConfigFacture>) => {
          this.appConfig.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          
          resolve(result)
        },

        error => {
          this.appConfig.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })
  }

}
