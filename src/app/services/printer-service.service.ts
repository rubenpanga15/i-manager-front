import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpUrls } from '../utilities/http-urls';
import { AppConfigService } from './app-config.service';
import { Utility } from '../utilities/utility';
import { AppConst } from '../utilities/app-const';

@Injectable({
  providedIn: 'root'
})
export class PrinterService {

  constructor(
    private appConfig: AppConfigService,
    private dialog: MatDialog,
    private httpClient: HttpClient
  ) { }

  public printFacture(data: string): void{
    this.appConfig.onStartWaiting('Impression de la facture en cours...');
    this.httpClient.post(HttpUrls.URL_PRINT_FACTURE, data)
    .subscribe(
      (result: HttpDataResponse<string>) => {
        this.appConfig.onStopWaiting();
        if(result.error.errorCode == 'KO'){
          Utility.openInfoDialog(this.dialog, result.error.errorDescription);
          return;
        }
        let w = window.open("data:application/pdf;base64, " + result.response);
        w.name = "Facture";
        
        setTimeout(() => {
          w.print();
          w.close();
        }, 1000);
        },
      error => {
        this.appConfig.onStopWaiting()
        Utility.openInfoDialog(this.dialog, AppConst.NETWORK_ERROR)
      }
    )
  }

  public printCommande(data: string): void{
    this.appConfig.onStartWaiting('Impression de la commande en cours...');
    this.httpClient.post(HttpUrls.URL_PRINT_COMMANDE, data)
    .subscribe(
      (result: HttpDataResponse<string>) => {
        this.appConfig.onStopWaiting();
        if(result.error.errorCode == 'KO'){
          Utility.openInfoDialog(this.dialog, result.error.errorDescription);
          return;
        }
        let w = window.open("data:application/pdf;base64, " + result.response);
        w.name = "Facture";
        
        setTimeout(() => {
          w.print();
          w.close();
        }, 1000);
        },
      error => {
        this.appConfig.onStopWaiting()
        Utility.openInfoDialog(this.dialog, AppConst.NETWORK_ERROR)
      }
    )
  }
}
