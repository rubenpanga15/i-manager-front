import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { User } from '../entities/user';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { WaitingDialogComponent } from '../dialogs/waiting-dialog/waiting-dialog.component'
import { HttpDataResponse } from '../utilities/http-data-response';
import { HttpUrls } from '../utilities/http-urls';
import { ErrorResponse } from '../utilities/error-response';
import { AppConst } from '../utilities/app-const';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {

  user: User;
  user$ : Subject<User> = new Subject<User>();
  private dialogRef: MatDialogRef<WaitingDialogComponent>;

  constructor(
    private dialog: MatDialog,
    private httpClient: HttpClient,
  ) { }

  onStartWaiting(message: string = 'Traitement en cours...'):void{
    
    this.dialogRef = this.dialog.open(WaitingDialogComponent, {
      data: message
    });
  }

  onStopWaiting(): void{
    this.dialogRef.close();
  }

  onConnected(userConnected: User): void{
    this.user = userConnected;
    this.user$.next(userConnected);
  }

  onDisconnect(): void{
    this.user = undefined;
    this.user$.next(undefined);
  }

  public connexion(data: string): Promise<HttpDataResponse<User>>{

    return new Promise<HttpDataResponse<User>>((resolve, reject) => {
      this.onStartWaiting('Authentification en cours ...');
      this.httpClient.post(HttpUrls.URL_CONNEXION, data)
      .subscribe(
        (result: HttpDataResponse<User>) => {
          this.onStopWaiting();
          if(result.error.errorCode === ErrorResponse.KO)
            reject(result.error.errorDescription);
          this.onConnected(result.response);
          resolve(result);
        },
        error => {
          this.onStopWaiting();
          reject(AppConst.NETWORK_ERROR);
        }
      )
    })

  }


}
