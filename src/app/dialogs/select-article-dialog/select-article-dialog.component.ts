import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Article } from 'src/app/entities/article';
import { CategorieArticle } from 'src/app/entities/categorie-article';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-select-article-dialog',
  templateUrl: './select-article-dialog.component.html',
  styleUrls: ['./select-article-dialog.component.css']
})
export class SelectArticleDialogComponent implements OnInit {

  categories: CategorieArticle[];
  articles: Article[]

  fkCategorie: number;
  motClef: string = "";

  constructor(
    private appConfig: AppConfigService,
    private service: ConfigurationService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SelectArticleDialogComponent>
  ) { }

  ngOnInit(): void {

    this.relaoad();

  }

  filtreData(): Article[]{
    if(!this.articles) return []
    return this.articles.filter(e => {
      if(this.fkCategorie)
       return e.fkCategorieArticle == this.fkCategorie && e.nom.toLowerCase().includes(this.motClef)
      else
        return e.nom.toLowerCase().includes(this.motClef)
    });
  }

  async relaoad(){
    await this.service.getListeArticles()
    .then(
      result => this.articles = result.response
    ).catch(
      error => {Utility.openInfoDialog(this.dialog, error)}
    )

    await this.service.getListCategorieArticle()
    .then(
      result => {
        this.categories = result.response
      }
    ).catch(
      error => {Utility.openInfoDialog(this.dialog, error)}
    )
  }

  select(item: Article): void{
    this.dialogRef.close(item);
  }



}
