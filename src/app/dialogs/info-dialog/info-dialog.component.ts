import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'app-info-dialog',
  templateUrl: './info-dialog.component.html',
  styleUrls: ['./info-dialog.component.css']
})
export class InfoDialogComponent implements OnInit {

  message: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) data: string
  ) { 
    this.message = data;
  }

  ngOnInit(): void {
  }

}
