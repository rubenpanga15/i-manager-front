import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog'
import { Article } from 'src/app/entities/article';
import { DetailCommande } from 'src/app/entities/detail-commande';
import { Utility } from 'src/app/utilities/utility';
import { SelectArticleDialogComponent } from '../select-article-dialog/select-article-dialog.component';

@Component({
  selector: 'app-ajouter-article-dialog',
  templateUrl: './ajouter-article-dialog.component.html',
  styleUrls: ['./ajouter-article-dialog.component.css']
})
export class AjouterArticleDialogComponent implements OnInit {

  detail: DetailCommande = new DetailCommande();
  volume: number = 1;

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AjouterArticleDialogComponent, DetailCommande>
  ) { }

  ngOnInit(): void {
    this.detail.volume = 0;
    this.detail.montant = 0;
  }


  openChoisirArticle(): void{
    this.dialog.open(SelectArticleDialogComponent, {
      width: '80%',
      height: '90%'
    }).afterClosed().subscribe(
      data => {
        this.detail.article = data;
        this.detail.fkArticle = data.id;
        this.detail.prixUnitaire = data.tarif.montant;
        this.detail.isAccompagnement = false;
      }
    )
  }

  calcul(): void{
    this.detail.montant = this.detail.prixUnitaire * this.detail.volume;
    if(this.detail.article.remiseApartirDe <= this.detail.volume && this.detail.article.remiseApartirDe > 0){
      if(this.detail.article.isRemisePourcentage)
        this.detail.montant = this.detail.montant - (this.detail.montant * this.detail.article.remise/100);
      else
        this.detail.montant = this.detail.montant - this.detail.article.remise;
    }
  }

  isAccopagnement(): void{
    if(this.detail.isAccompagnement){
      this.detail.prixUnitaire = 0;
      this.detail.montant = 0
    }
    else{
      this.detail.prixUnitaire = this.detail.article.tarif.montant;
      this.detail.montant = this.detail.prixUnitaire * this.detail.volume;
    }
  }



  add(): void{

    console.log(this.detail);

    if(!this.detail.fkArticle){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner l'article");
      return;
    }

    if(!this.detail.prixUnitaire && !this.detail.isAccompagnement){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le prix unitaire");
      return;
    }

    if(!this.detail.volume){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le volume");
      return;
    }

    if(this.detail.volume > this.detail.article.stockRestant){
      Utility.openInfoDialog(this.dialog, "Le stock est insuffisant");
      return;
    }

    if(!this.detail.fkArticle){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner l'artcile");
      return;
    }

    if(!this.detail.montant && !this.detail.isAccompagnement){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le montant");
      return;
    }

    this.dialogRef.close(this.detail);

  }

}
