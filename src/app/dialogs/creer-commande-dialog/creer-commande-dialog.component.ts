import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog'
import { SelectServeurDialogComponent } from '../select-serveur-dialog/select-serveur-dialog.component';

@Component({
  selector: 'app-creer-commande-dialog',
  templateUrl: './creer-commande-dialog.component.html',
  styleUrls: ['./creer-commande-dialog.component.css']
})
export class CreerCommandeDialogComponent implements OnInit {

  serveur: string

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  openSelectionnerServeur(): void{
    this.dialog.open(SelectServeurDialogComponent, {
      width: '90%',
      height: '80%'
    }).afterClosed().subscribe(
      data => this.serveur = "SERVEUR 1"
    )
  }

}
