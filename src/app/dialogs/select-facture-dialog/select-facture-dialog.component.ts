import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Facture } from 'src/app/entities/facture';
import { AppConfigService } from 'src/app/services/app-config.service';
import { CommandeService } from 'src/app/services/commande.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-select-facture-dialog',
  templateUrl: './select-facture-dialog.component.html',
  styleUrls: ['./select-facture-dialog.component.css']
})
export class SelectFactureDialogComponent implements OnInit {

  factures: Facture[];
  motClef: string = '';

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SelectFactureDialogComponent>,
    private appConfig: AppConfigService,
    private service: CommandeService,
    @Inject(MAT_DIALOG_DATA) data: any
  ) { }

  ngOnInit(): void {
    this.service.getListeFacturesParEtat(JSON.stringify({etat: 'SOLDE'}))
    .then(
      res => this.factures = res.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  filterData(): Facture[]{
    return this.factures.filter(
      e => e.client.toLowerCase().includes(this.motClef.toLowerCase()) 
      || e.table.toLowerCase().includes(this.motClef.toLowerCase())
      || e.numero.includes(this.motClef.toLowerCase())
      || e.user.noms.toLowerCase().includes(this.motClef.toLowerCase())
    )
  }

  select(facture: Facture): void{
    this.dialogRef.close(facture);
  }

}
