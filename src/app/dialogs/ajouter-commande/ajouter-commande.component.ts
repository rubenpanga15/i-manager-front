import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Commande } from '../../entities/commande';
import { AppConfigService } from '../../services/app-config.service';
import { CommandeService } from '../../services/commande.service';
import { Utility } from '../../utilities/utility';

@Component({
  selector: 'app-ajouter-commande',
  templateUrl: './ajouter-commande.component.html',
  styleUrls: ['./ajouter-commande.component.css']
})
export class AjouterCommandeComponent implements OnInit {

  commandes: Commande[]=[];
  seleted: Commande[]=[];

  motClef: string = '';

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<AjouterCommandeComponent>,
    private appConfig: AppConfigService,
    private service: CommandeService
  ) { }

  filterData(): Commande[]{
    if(!this.commandes || this.commandes.length <= 0) return []

    return this.commandes.filter( e => e.numero.toLowerCase().includes(this.motClef.toLowerCase()) || e.user.noms.toLowerCase().includes(this.motClef.toLowerCase()) || (e.client && e.client.toLowerCase().includes(this.motClef.toLowerCase())) || e.table.toLowerCase().includes(this.motClef.toLowerCase()));
  }

  ngOnInit(): void {

    this.service.getListeCommandeEnAttente()
    .then(
      result => this.commandes = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
    
  }

  add(commande: Commande): void{
      if(!this.isAlreadySelected(commande)){
        this.seleted.push(commande);
      }
      else
        this.remove(commande);
  }

  isAlreadySelected(commande: Commande): boolean{
    let isSelected: boolean = false;
    for(let i = 0; i < this.seleted.length; i++){
      if(this.seleted[i].numero === commande.numero)
        isSelected = true;
    }

    return isSelected;
  }

  remove(commande: Commande): void{
    for(let i = 0; i < this.seleted.length; i++){
      if(this.seleted[i].numero === commande.numero)
        this.seleted.splice(i, 1);
    }
  }

  close(): void{
    this.dialogRef.close(this.seleted);
  }

}
