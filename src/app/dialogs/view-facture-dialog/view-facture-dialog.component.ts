import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Commande } from 'src/app/entities/commande';
import { Facture } from 'src/app/entities/facture';
import { TauxDeChange } from 'src/app/entities/taux-de-change';
import { AppConfigService } from 'src/app/services/app-config.service';
import { CommandeService } from 'src/app/services/commande.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { PrinterService } from 'src/app/services/printer-service.service';
import { Utility } from 'src/app/utilities/utility';
import { ViewCommandeDialogComponent } from '../view-commande-dialog/view-commande-dialog.component';

@Component({
  selector: 'app-view-facture-dialog',
  templateUrl: './view-facture-dialog.component.html',
  styleUrls: ['./view-facture-dialog.component.css']
})
export class ViewFactureDialogComponent implements OnInit {

  facture: Facture;

  tauxUSD_CDF: TauxDeChange;
  tauxCDF_USD: TauxDeChange;

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ViewCommandeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: Facture,
    private appConfig: AppConfigService,
    private configService: ConfigurationService,
    private service: CommandeService,
    private printerService: PrinterService
  ) { 
    this.facture = data;
    console.log(data)
  }

  ngOnInit(): void {
    
  }

  sumUSD(): number{
    let montantUSD =this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantCDF = this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantUSD +=  montantCDF / this.facture.tauxDeChange ;

    return montantUSD;
  }

  tva(): number{
    return Number.parseFloat((this.facture.montantTotal * 16/100).toFixed(3));
  }

  sumCDF(): number{
    return Number.parseFloat((this.facture.montantTotal * this.facture.tauxDeChange).toFixed(3));
  }

  print(): void{
    this.printerService.printFacture(JSON.stringify({numero: this.facture.numero}))
  }

  sumUSDWithoutTva(): number{
    let montantUSD =this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantCDF = this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantUSD +=  montantCDF / this.facture.tauxDeChange ;

    if(this.facture.remise > 0){
      if(this.facture.isRemisePourcentage)
        montantUSD = montantUSD - (montantUSD * this.facture.remise/100);
      else
        montantUSD = montantUSD - this.facture.remise;
    }
    return montantUSD;
  }

}
