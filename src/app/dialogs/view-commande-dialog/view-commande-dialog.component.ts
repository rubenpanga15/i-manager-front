import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Commande } from 'src/app/entities/commande';
import { TauxDeChange } from 'src/app/entities/taux-de-change';
import { AppConfigService } from 'src/app/services/app-config.service';
import { CommandeService } from 'src/app/services/commande.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { PrinterService } from 'src/app/services/printer-service.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-view-commande-dialog',
  templateUrl: './view-commande-dialog.component.html',
  styleUrls: ['./view-commande-dialog.component.css']
})
export class ViewCommandeDialogComponent implements OnInit {

  commande: Commande;

  tauxUSD_CDF: TauxDeChange;
  tauxCDF_USD: TauxDeChange;

  
  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<ViewCommandeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: Commande,
    private appConfig: AppConfigService,
    private configService: ConfigurationService,
    private service: CommandeService,
    private printerService: PrinterService
  ) {
    this.commande = data;
   }

  ngOnInit(): void {
    this.reload();
  }

  async reload(){
    await this.configService.getTauxDeChange(JSON.stringify({fkDeviseFrom: 'CDF', fkDeviseTo: 'USD'}))
    .then(
      result => this.tauxCDF_USD = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    );

    await this.configService.getTauxDeChange(JSON.stringify({fkDeviseFrom: 'USD', fkDeviseTo: 'CDF'}))
    .then(
      result => this.tauxUSD_CDF = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    );
  }

  sumUSD(): number{
    let montantUSD =this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantCDF = this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantUSD +=  montantCDF / this.tauxUSD_CDF.taux ;

    return montantUSD;
  }

  sumCDF(): number{
    let montantCDF =this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantUSD = this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantCDF +=  montantUSD * this.tauxUSD_CDF.taux ;

    return montantCDF;
  }

  print(): void{
    this.printerService.printCommande(JSON.stringify({numero: this.commande.numero}))
  }

}
