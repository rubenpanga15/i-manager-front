export class ErrorResponse {
    static KO: string = "KO";
    static OK: string = "OK";

    errorCode: string;
    errorDescription: string;
}
