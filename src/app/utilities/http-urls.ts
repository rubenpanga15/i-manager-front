export class HttpUrls {

    private static SCHEMAS: string = 'http';
    // private static IP: string = 'localhost';
    // private static PORT: string = '8089';
    private static IP: string = '102.68.62.41';
    private static PORT: string = '3000';

    private static getBase(): string{
        return HttpUrls.SCHEMAS + '://' + HttpUrls.IP + ':' + HttpUrls.PORT + '/i-manager/';
    }

    static URL_CONNEXION = HttpUrls.getBase() + 'api/connexion';
    static URL_EDIT_CATEGORIE_ARTICLE = HttpUrls.getBase() + 'api/configuration/edit-categorie';
    static URL_EDIT_DEVISE = HttpUrls.getBase() + 'api/configuration/edit-devise';
    static URL_EDIT_SITE = HttpUrls.getBase() + 'api/configuration/edit-site';
    static URL_EDIT_FOURNISSEUR = HttpUrls.getBase() + 'api/configuration/edit-fournisseur';
    static URL_EDIT_USER = HttpUrls.getBase() + 'api/configuration/edit-user';
    static ULR_EDIT_ARTICLE = HttpUrls.getBase() + 'api/configuration/edit-article';
    static URL_EDIT_TAUX_DECHANGE = HttpUrls.getBase() +'api/configuration/edit-taux';
    static URL_EDIT_TAXE = HttpUrls.getBase() +'api/configuration/edit-taxe';
    static URL_EDIT_CONFIG = HttpUrls.getBase() +'api/configuration/edit-config';
    static URL_GET_CONFIG = HttpUrls.getBase() +'api/configuration/get-config';
    static URL_GET_DEFAULT_DEVISE = HttpUrls.getBase() + 'api/configuration/get-default-devise';
    static URL_GET_TAUX = HttpUrls.getBase() + 'api/configuration/get-taux';


    static URL_SAVE_COMMANDE =  HttpUrls.getBase() + 'api/commande/save-commande';
    static URL_GET_COMMANDE_ATTENTE = HttpUrls.getBase() + 'api/commande/get-commande-attente';
    static URL_SAVE_FACTURE = HttpUrls.getBase() + 'api/commande/save-facture';

    static URL_PRINT_FACTURE = HttpUrls.getBase() + 'api/printer/print-facture';
    static URL_PRINT_COMMANDE = HttpUrls.getBase() + 'api/printer/print-commande';

    static URL_LISTE_FACTURES = HttpUrls.getBase() + 'api/facture/list-factures';
    static URL_LISTE_COMMANDES = HttpUrls.getBase() + 'api/commande/liste-commandes';
    static URL_LISTE_TODAY_FACTURES = HttpUrls.getBase() + 'api/facture/liste-today-factures';
    static URL_LISTE_TODAY_COMMANDE = HttpUrls.getBase();
    static URL_LISTE_FACTURE_ETAT = HttpUrls.getBase() + 'api/facture/get-liste-facture-etat';

    static URL_EFFECTUER_PAIEMENT = HttpUrls.getBase() + 'api/paiement/effectuer-paiement';
    static URL_LISTE_PAIEMENT = HttpUrls.getBase() + 'api/paiement/liste-paiement';
    static URL_LISTE_TODAY_PAIEMENT = HttpUrls.getBase() + 'api/paiement/ltoday-liste-paiements';
}
