import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Fournisseur } from 'src/app/entities/fournisseur';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';
import { Article } from 'src/app/entities/article';
import { Tarif } from 'src/app/entities/tarif';
import { CategorieArticle } from 'src/app/entities/categorie-article';
import { Devise } from 'src/app/entities/devise';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css'],
})
export class EditArticleComponent implements OnInit {

  article: Article
  articles: Article[]

  categories: CategorieArticle[]
  devises: Devise[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    this.article = new Article();
    this.article.description = '';
    this.article.fkSite = this.user.fkSite;
    this.article.tarif = new Tarif();
    this.article.tarif.fkSite = this.user.fkSite;
    this.article.remise = 0;
    this.article.isRemisePourcentage = true;
    this.article.remiseApartirDe = 0;
    this.article.tarif = new Tarif();
  }

  filtreData(): Article[]{
    if(!this.articles) return []
    return this.articles.filter(e => e.nom.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  async relaoad(){
    await this.service.getListeArticles()
    .then(
      result => this.articles = result.response
    ).catch(
      error => {Utility.openInfoDialog(this.dialog, error)}
    )

    await this.service.getListCategorieArticle()
    .then(
      result => {
        this.categories = result.response
      }
    ).catch(
      error => {Utility.openInfoDialog(this.dialog, error)}
    )

    await this.service.getListeDevise()
    .then(
      result => this.devises = result.response
    ).catch(
      error => {}
    )

    
  }

  edit(item: Article): void{
    this.article = item;
  }

  resetArticle(): void{
    this.article = undefined;
  }

  valider(): void{
    this.article.tarif.fkSite = this.user.fkSite;
    if(!this.article.nom || this.article.nom.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le nom')
      return;
    }

    if(!this.article.code || this.article.code.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le code');
      return;
    }

    if(!this.article.fkCategorieArticle){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la categorie');
      return;
    }

    if(!this.article.uniteMesure || this.article.uniteMesure.length <= 0){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner l'unité de mesure");
      return;
    }

    if(!this.article.stockRestant || this.article.stockRestant <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le stock restant');
      return;
    }

    if(!this.article.stockAlert){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le stock d'alerte");
      return;
    }

    if(!this.article.tarif.montant || this.article.tarif.montant <= 0){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le prix unitaire");
      return;
    }

    if(!this.article.tarif.fkDevise){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner la devise");
      return;
    }

    this.service.editArticle(JSON.stringify(this.article))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

}
