import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AjouterArticleDialogComponent } from 'src/app/dialogs/ajouter-article-dialog/ajouter-article-dialog.component';
import { Article } from 'src/app/entities/article';
import { Commande } from 'src/app/entities/commande';
import { DetailCommande } from 'src/app/entities/detail-commande';
import { TauxDeChange } from 'src/app/entities/taux-de-change';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { HttpDataResponse } from 'src/app/utilities/http-data-response';
import { Utility } from 'src/app/utilities/utility';
import { CommandeService } from 'src/app/services/commande.service';
import { Devise } from 'src/app/entities/devise';
import { ViewCommandeDialogComponent } from 'src/app/dialogs/view-commande-dialog/view-commande-dialog.component';

@Component({
  selector: 'app-edit-commande',
  templateUrl: './edit-commande.component.html',
  styleUrls: ['./edit-commande.component.css']
})
export class EditCommandeComponent implements OnInit {

  details: DetailCommande[] = [];
  commande: Commande = new Commande();

  user: User;

  tauxUSD_CDF: TauxDeChange;
  tauxCDF_USD: TauxDeChange;
  defaultDevise: Devise;

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private configService: ConfigurationService,
    private service: CommandeService
  ) { 
    this.user = appConfig.user;
    this.commande.fkSite = this.user.fkSite;
    this.commande.user = this.user;
    this.commande.details = [];
    this.commande.fkUser = this.user.id;
  }

  clear(): void{
    this.commande = new Commande();
    this.commande.fkSite = this.user.fkSite;
    this.commande.user = this.user;
    this.commande.details = [];
    this.commande.fkUser = this.user.id;
  }

  ngOnInit(): void {
    this.reload();
  }

  async reload(){
    await this.configService.getTauxDeChange(JSON.stringify({fkDeviseFrom: 'CDF', fkDeviseTo: 'USD'}))
    .then(
      result => this.tauxCDF_USD = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    );

    await this.configService.getTauxDeChange(JSON.stringify({fkDeviseFrom: 'USD', fkDeviseTo: 'CDF'}))
    .then(
      result => this.tauxUSD_CDF = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    );

    await this.configService.getDefaultDevise()
    .then(
      result => {
        this.defaultDevise = result.response
        this.commande.fkDefaultDevise = this.defaultDevise.code;
      }
    );
    await this.configService.getDefaultDevise()
    .then(
      result => {
        this.defaultDevise = result.response
        this.commande.fkDefaultDevise = this.defaultDevise.code;
      }
    );
  }

  openAjouterArticle(): void{
    this.dialog.open<AjouterArticleDialogComponent, any, DetailCommande>(AjouterArticleDialogComponent, {
      width: '80%',
      height: '90%'
    }).afterClosed().subscribe(
      (data: DetailCommande) => {
        if(data.article)
          this.commande.details.push(data)
      }
    )
  }

  deleteItem(index: number): void{
    this.commande.details.splice(index, 1);
  }

  sumUSD(): number{
    let montantUSD =this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantCDF = this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantUSD +=  montantCDF / this.tauxUSD_CDF.taux ;

    return montantUSD;
  }

  sumCDF(): number{
    let montantCDF =this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantUSD = this.commande.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantCDF +=  montantUSD * this.tauxUSD_CDF.taux ;

    return montantCDF;
  }


  cloturer(): void{

    if(this.defaultDevise.code == 'CDF')
      this.commande.montantTotal = this.sumCDF();
    if(this.defaultDevise.code == 'USD')
      this.commande.montantTotal = this.sumUSD();

    if(!this.commande.table || this.commande.table.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la table');
      return;
    }

    if(!this.commande.details || this.commande.details.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Aucun article commandé');
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûre de vouloir enregistrer cette commande?')
    .afterClosed()
    .subscribe(
      res => {
        if(res == 'OK'){
          this.service.saveCommande(JSON.stringify(this.commande))
          .then(
            result => {
              this.commande.numero = result.response
              this.dialog.open(ViewCommandeDialogComponent, {
                width: '90%',
                height: '90%',
                data: this.commande
              })
              this.clear();
              
            }
          ).catch(error => Utility.openInfoDialog(this.dialog,error))
        }
      }
    )

  }

}
