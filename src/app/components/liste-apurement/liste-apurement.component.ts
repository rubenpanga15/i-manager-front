import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewFactureDialogComponent } from 'src/app/dialogs/view-facture-dialog/view-facture-dialog.component';
import { Facture } from 'src/app/entities/facture';
import { Paiement } from 'src/app/entities/paiement';
import { AppConfigService } from 'src/app/services/app-config.service';
import { PaiementService } from 'src/app/services/paiement.service';

@Component({
  selector: 'app-liste-apurement',
  templateUrl: './liste-apurement.component.html',
  styleUrls: ['./liste-apurement.component.css']
})
export class ListeApurementComponent implements OnInit {

  data = {
    dateDebut: '',
    dateFin: '',
  }

  motclef: string = '';
  message:string = '';
  paiements: Paiement[] = []

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: PaiementService
  ) { }

  ngOnInit(): void {
    this.service.getTodayListePaiements()
    .then(
      res => this.paiements = res.response
    ).catch(
      error => console.log(error)
    )
  }

  filterData(): Paiement[]{
    return this.paiements.filter(
      e => e.client.toLowerCase().includes(this.motclef) || e.fkFacture.includes(this.motclef) || e.user.noms.toLowerCase().includes(this.motclef)
    )
  }

  viewFacture(facture: Facture): void{
    this.dialog.open(ViewFactureDialogComponent, {
      width: '90%',
      height: '80%',
      data: facture
    });
  }

}
