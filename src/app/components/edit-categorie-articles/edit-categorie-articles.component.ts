import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CategorieArticle } from 'src/app/entities/categorie-article'
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-edit-categorie-articles',
  templateUrl: './edit-categorie-articles.component.html',
  styleUrls: ['./edit-categorie-articles.component.css']
})
export class EditCategorieArticlesComponent implements OnInit {

  categorie: CategorieArticle
  categories: CategorieArticle[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    console.log(this.appConfig.user);
    this.categorie = new CategorieArticle();
    this.categorie.description = ''
    this.categorie.fkSite = this.user.fkSite;
  }

  filtreData(): CategorieArticle[]{
    if(!this.categories) return [];
    return this.categories.filter(e => e.description.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  relaoad(): void{
    this.service.getListCategorieArticle()
    .then(
      result => this.categories = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: CategorieArticle): void{
    this.categorie = item;
  }

  resetArticle(): void{
    this.categorie = undefined;
  }

  valider(): void{
    if(!this.categorie.description || this.categorie.description.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la designation');
      return;
    }
    this.service.editCategorie(JSON.stringify(this.categorie))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openErrorDialog(this.dialog, error))
  }

}
