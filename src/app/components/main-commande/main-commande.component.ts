import { Component, OnInit } from '@angular/core';
import { Table } from 'src/app/entities/table';
import { Utility } from 'src/app/utilities/utility'
import { MatDialog } from '@angular/material/dialog';
import { TableDialogComponent } from 'src/app/dialogs/table-dialog/table-dialog.component'
import { CreerCommandeDialogComponent } from 'src/app/dialogs/creer-commande-dialog/creer-commande-dialog.component';
import { AjouterArticleDialogComponent } from 'src/app/dialogs/ajouter-article-dialog/ajouter-article-dialog.component';

@Component({
  selector: 'app-main-commande',
  templateUrl: './main-commande.component.html',
  styleUrls: ['./main-commande.component.css']
})
export class MainCommandeComponent implements OnInit {

  table: string;
  commande: string;
  motantTotal: number = 0;

  articles = [];

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  choisirTable(): void {
     this.dialog.open(TableDialogComponent, {
       width: '80%',
       height: '90%'
     }).afterClosed().subscribe(
       data => this.table = "TABLE 1"
     )
  }

  openCreerCommande(): void{
    this.dialog.open(CreerCommandeDialogComponent, {
      width: '80%',
      height: '90%'
    }).afterClosed().subscribe(
      data => this.commande = 'commande'
    )
  }

  openAjouterArticle(): void{
    this.dialog.open(AjouterArticleDialogComponent, {
      width: '80%',
      height: '90%'
    }).afterClosed().subscribe(
      data => {
        this.articles.push({
          designation: 'COCA-COLA',
          volume: '4 Bouteille',
          prixUnitaire: '10 USD',
          montant: '40 USD'
        });

        this.motantTotal = this.articles.length * 10
      }
    )
  }

  askCloturer(): void{
    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr(e) de vouloir cloturer la commande et produire la facture?').afterClosed().subscribe(
      data => Utility.openSuccessDialog(this.dialog, 'Facture créée avec succès')
    );
  }

}
