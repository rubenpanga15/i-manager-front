import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  user: User;

  constructor(
    private appConfig: AppConfigService
  ) { 
    this.user = appConfig.user;
  }

  ngOnInit(): void {
  }

}
