import { flatten } from '@angular/compiler';
import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AjouterCommandeComponent } from 'src/app/dialogs/ajouter-commande/ajouter-commande.component';
import { ViewCommandeDialogComponent } from 'src/app/dialogs/view-commande-dialog/view-commande-dialog.component';
import { Commande } from 'src/app/entities/commande';
import { ConfigFacture } from 'src/app/entities/config-facture';
import { DetailCommande } from 'src/app/entities/detail-commande';
import { DetailFacture } from 'src/app/entities/detail-facture';
import { Devise } from 'src/app/entities/devise';
import { Facture } from 'src/app/entities/facture';
import { TauxDeChange } from 'src/app/entities/taux-de-change';
import { Taxe } from 'src/app/entities/taxe';
import { AppConfigService } from 'src/app/services/app-config.service';
import { CommandeService } from 'src/app/services/commande.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';
import { PrinterService } from 'src/app/services/printer-service.service';

@Component({
  selector: 'app-edit-facture',
  templateUrl: './edit-facture.component.html',
  styleUrls: ['./edit-facture.component.css']
})
export class EditFactureComponent implements OnInit {
  facture: Facture = new Facture();

  config: ConfigFacture;
  defaultDevise: Devise;
  taxes: Taxe[];
  tauxUSD_CDF: TauxDeChange;
  tauxCDF_USD: TauxDeChange;

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: CommandeService,
    private configService: ConfigurationService,
    private printerService: PrinterService
  ) { 
    this.clear();
  }

  async reload(){
    await this.configService.getConfig(JSON.stringify({fkSite: this.facture.fkSite }))
    .then(
      res => this.config = res.response
    ).catch( error => Utility.openInfoDialog(this.dialog, error))

    await this.configService.getDefaultDevise()
    .then(
      res => this.defaultDevise = res.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )

    await this.configService.getListeTaxe()
    .then(
      res => this.taxes = res.response
    );

    await this.configService.getTauxDeChange(JSON.stringify({fkDeviseFrom: 'CDF', fkDeviseTo: 'USD'}))
    .then(
      result => {
        this.tauxCDF_USD = result.response
        
      }
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    );

    await this.configService.getTauxDeChange(JSON.stringify({fkDeviseFrom: 'USD', fkDeviseTo: 'CDF'}))
    .then(
      result => {this.tauxUSD_CDF = result.response; this.facture.tauxDeChange = this.tauxUSD_CDF.taux}
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    );
  }

  ngOnInit(): void {
    this.reload();
  }

  openAjouterCommande(): void{
    this.dialog.open(AjouterCommandeComponent, {
      width: '90%',
      height: '80%'
    }).afterClosed()
    .subscribe(
      (result: Commande[]) => {
        this.facture.details = [];
        this.facture.commandes = [];
        if(result){
          this.facture.commandes = result;
          for(let i = 0; i < result.length; i++){
            this.facture.details = [...this.facture.details, ...this.convertDetailCommandeToDetailFacture(result[i].details)]
          }
        }
      }
    )
  }

  convertDetailCommandeToDetailFacture(data: DetailCommande[]): DetailFacture[]{
    return data.map(e => {
      let detail: DetailFacture = new DetailFacture();

      detail.article = e.article;
      detail.fkArticle = e.fkArticle;
      detail.dateCreat = e.dateCreat;
      detail.isAccompagnement = e.isAccompagnement;
      detail.montant = e.montant;
      detail.prixUnitaire = e.prixUnitaire;
      detail.status = e.status;
      detail.volume = e.volume;

      return detail;
    })
  }

  sumUSD(): number{
    let montantUSD =this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantCDF = this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantUSD +=  montantCDF / this.tauxUSD_CDF.taux ;

    if(this.facture.remise > 0){
      if(this.facture.isRemisePourcentage)
        montantUSD = montantUSD - (montantUSD * this.facture.remise/100);
      else
        montantUSD = montantUSD - this.facture.remise;
    }

    if(this.facture.applyTva){
      montantUSD += montantUSD * 14/100 
    }

    return Number.parseFloat(montantUSD.toFixed(3));
  }

  sumUSDWithoutTva(): number{
    let montantUSD =this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'USD')
        return previous + current.montant
      else
        return previous
    }, 0);

    let montantCDF = this.facture.details.reduce<number>((previous, current) => {
      if(current.article.tarif.fkDevise == 'CDF')
        return previous + current.montant
      else
        return previous
    }, 0);

    montantUSD +=  montantCDF / this.tauxUSD_CDF.taux ;

    if(this.facture.remise > 0){
      if(this.facture.isRemisePourcentage)
        montantUSD = montantUSD - (montantUSD * this.facture.remise/100);
      else
        montantUSD = montantUSD - this.facture.remise;
    }
    return montantUSD;
  }

  tva(): number{
    return Number.parseFloat((this.sumUSDWithoutTva() * 16/100).toFixed(3));
  }

  sumCDF(): number{

    return this.sumUSD() * this.tauxUSD_CDF.taux ;
  }

  save(): void{

    this.facture.montantTotal = this.sumUSD();
    if(this.facture.applyTva)
      this.facture.tva = this.tva();
    this.facture.fkDefaultDevise = this.defaultDevise.code;

    if(!this.facture.details || this.facture.details.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez selectionner les commandes à facturer');
      return;
    }

    if(this.facture.montantTotal <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le montantTotal');
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûre de vouloir enregistrer cette commande?')
    .afterClosed()
    .subscribe(
      res => {
        if(res == 'OK'){
          this.service.saveFacture(JSON.stringify(this.facture))
          .then(
            result => {

              this.printerService.printFacture(JSON.stringify({numero: result.response}))

              this.clear();
              
            }
          ).catch(error => Utility.openInfoDialog(this.dialog,error))
        }
      }
    )

  }

  print(): void{
    this.printerService.printFacture(JSON.stringify({numero: '00007'}))
  }

  clear(): void{
    this.facture.details = [];
    this.facture.commandes = [];
    this.facture.user = this.appConfig.user;
    this.facture.fkSite = this.appConfig.user.fkSite;
    this.facture.fkUser = this.appConfig.user.id;
    this.facture.applyTva = false;
    this.facture.isRemisePourcentage = true;
    this.facture.remise = 0;
    this.facture.tva = 0;
    this.facture.table = '';
    this.facture.client = '';
    
  }

}
