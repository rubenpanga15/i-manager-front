import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ViewFactureDialogComponent } from 'src/app/dialogs/view-facture-dialog/view-facture-dialog.component';
import { Facture } from 'src/app/entities/facture';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { CommandeService } from 'src/app/services/commande.service';
import { PrinterService } from 'src/app/services/printer-service.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-liste-factures',
  templateUrl: './liste-factures.component.html',
  styleUrls: ['./liste-factures.component.css']
})
export class ListeFacturesComponent implements OnInit {

  user: User
  message: String;
  factures: Facture[];

  motclef: string = '';

  data = {
    dateDebut: '',
    dateFin: '',
    etat: 'ATTENTE'
  }

  constructor(
    private appConfig: AppConfigService,
    private service: CommandeService,
    private printerService: PrinterService,
    private dialog: MatDialog
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.service.getListeFacturesToday()
    .then(
      res => this.factures = res.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  filterData(): Facture[]{
    return this.factures.filter(
      e => e.numero.includes(this.motclef) || e.user.noms.toLowerCase().includes(this.motclef.toLowerCase()) || e.client.toLowerCase().includes(this.motclef.toLowerCase())  || e.table.toLowerCase().includes(this.motclef.toLowerCase())
    )
  }

  printFacture(numero: string): void{
    this.printerService.printFacture(JSON.stringify({numero: numero}));
  }

  viewFacture(facture: Facture): void{
    this.dialog.open(ViewFactureDialogComponent, {
      width: '90%',
      height: '80%',
      data: facture
    })
  }

  getFacture(): void{
    this.factures = []
    this.service.getListeFactures(JSON.stringify(this.data))
    .then(
      result => {
        this.factures = result.response;
        this.message = result.error.errorDescription;
      }
    ).catch(
      error => this.message = error
    )
  }

}
