import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../../entities/user';
import { AppConfigService } from '../../services/app-config.service';
import { ConfigurationService } from '../../services/configuration.service';
import { Utility } from '../../utilities/utility';
import { Devise } from '../../entities/devise';
import { Taxe } from '../../entities/taxe'

@Component({
  selector: 'app-edit-taxe',
  templateUrl: './edit-taxe.component.html',
  styleUrls: ['./edit-taxe.component.css']
})
export class EditTaxeComponent implements OnInit {

  taxe: Taxe;
  taxes: Taxe[]
  devises: Devise[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    this.taxe = new Taxe();
    this.taxe.description = ''
    this.taxe.isPourcentage = false;
    this.taxe.montant = 0;
    this.taxe.fkSite = this.user.fkSite;
  }

  filtreData(): Taxe[]{
    if(!this.taxes) return []
    return this.taxes.filter(e => e.description.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  async relaoad(){
    await this.service.getListeTaxe()
    .then(
      result => this.taxes = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )

    await this.service.getListeDevise()
    .then(
      result => this.devises = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: Taxe): void{
    this.taxe = item;
  }

  resetArticle(): void{
    this.taxe = undefined;
  }

  valider(): void{
    if(!this.taxe.description || this.taxe.description.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la designation');
      return;
    }

    if(!this.taxe.montant || this.taxe.montant <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le taux de la taxe');
      return;
    }

    if(!this.taxe.fkDevise && !this.taxe.isPourcentage){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la devise');
      return;
    }

    if(!this.taxe.fkSite){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le site');
      return;
    }

    this.service.editTaxe(JSON.stringify(this.taxe))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openErrorDialog(this.dialog, error))
  }

}
