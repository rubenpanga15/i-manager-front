import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AppConfigService } from 'src/app/services/app-config.service';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  data = {
    username: '',
    password: '',
  }

  message: string

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private appConfig: AppConfigService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void{
    this.appConfig.connexion(JSON.stringify(this.data))
    .then(
      result => this.router.navigate(['/main'])
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

}
