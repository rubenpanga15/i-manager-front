import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Devise } from 'src/app/entities/devise';
import { Taxe } from 'src/app/entities/taxe';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';
import { ConfigFacture } from '../../entities/config-facture';

@Component({
  selector: 'app-edit-config-tarif',
  templateUrl: './edit-config-tarif.component.html',
  styleUrls: ['./edit-config-tarif.component.css']
})
export class EditConfigTarifComponent implements OnInit {

  config: ConfigFacture;
  devises: Devise[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }

  async relaoad(){
    await this.service.getConfig(JSON.stringify({fkSite: this.user.fkSite}))
    .then(
      result => this.config = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )

    await this.service.getListeDevise()
    .then(
      result => this.devises = result.response
    ).catch(
      error => {}
    )
  }

  edit(item: ConfigFacture): void{
    this.config = item;
  }

  resetArticle(): void{
    this.config = undefined;
  }

  valider(): void{
    
    this.service.editConfig(JSON.stringify(this.config))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

}
