import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';
import { Devise } from 'src/app/entities/devise'

@Component({
  selector: 'app-edit-devise',
  templateUrl: './edit-devise.component.html',
  styleUrls: ['./edit-devise.component.css']
})
export class EditDeviseComponent implements OnInit {

  devise: Devise
  devises: Devise[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    console.log(this.appConfig.user);
    this.devise = new Devise();
    this.devise.description = ''
    this.devise.fkSite = this.user.fkSite;
  }

  filtreData(): Devise[]{
    if(!this.devises) return []
    return this.devises.filter(e => e.description.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  relaoad(): void{
    this.service.getListeDevise()
    .then(
      result => this.devises = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: Devise): void{
    this.devise = item;
  }

  resetArticle(): void{
    this.devise = undefined;
  }

  valider(): void{
    if(!this.devise.code || this.devise.code.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le code');
      return;
    }
    if(!this.devise.description || this.devise.description.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la designation');
      return;
    }
    this.service.editDevise(JSON.stringify(this.devise))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openErrorDialog(this.dialog, error))
  }

}
