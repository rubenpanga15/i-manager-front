import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from '../../entities/user';
import { AppConfigService } from '../../services/app-config.service';
import { ConfigurationService } from '../../services/configuration.service';
import { Utility } from '../../utilities/utility';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  userEdit: User
  users: User[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    this.userEdit = new User();
    this.userEdit.fkSite = this.user.fkSite;
    this.userEdit.sexe = 'M';
  }

  filtreData(): User[]{
    if(!this.users) return []
    return this.users.filter(e => e.noms.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  relaoad(): void{
    this.service.getListeUsers()
    .then(
      result => this.users = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: User): void{
    this.userEdit = item;
  }

  resetArticle(): void{
    this.userEdit = undefined;
  }

  valider(): void{
    if(!this.userEdit.noms || this.userEdit.noms.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le nom');
      return;
    }

    if(!this.userEdit.sexe || this.userEdit.sexe.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le sexe');
      return;
    }

    if(!this.userEdit.telephone || this.userEdit.telephone.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le téléphone');
      return;
    }

    if(!this.userEdit.username || this.userEdit.username.length <= 0){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le nom d'utilisateur");
      return;
    }

    if(!this.userEdit.password || this.userEdit.password.length <= 0){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le mot de passe");
      return;
    }

    if(!this.userEdit.adresse || this.userEdit.adresse.length <= 0){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner l'adresse");
      return;
    }

    if(!this.userEdit.role || this.userEdit.role.length <= 0){
      Utility.openInfoDialog(this.dialog, "Veuillez renseigner le role");
      return;
    }

    this.service.editUser(JSON.stringify(this.userEdit))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openErrorDialog(this.dialog, error))
  }

}
