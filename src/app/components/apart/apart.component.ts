import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AjouterArticleDialogComponent } from 'src/app/dialogs/ajouter-article-dialog/ajouter-article-dialog.component';
import { Article } from 'src/app/entities/article';
import { Utility } from 'src/app/utilities/utility';

@Component({
  selector: 'app-apart',
  templateUrl: './apart.component.html',
  styleUrls: ['./apart.component.css']
})
export class ApartComponent implements OnInit {

  articles: Article[]
  montanTotal: number;

  constructor(
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
  }

  openAjouterArticle(): void{
    this.dialog.open(AjouterArticleDialogComponent, {
      width: '80%',
      height: '90%'
    }).afterClosed().subscribe(
      data => {
      }
    )
  }

  askCloturer(): void{
    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr(e) de vouloir cloturer la commande et produire la facture?').afterClosed().subscribe(
      data => Utility.openSuccessDialog(this.dialog, 'Facture créée avec succès')
    );
  }

}
