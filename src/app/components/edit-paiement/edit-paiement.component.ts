import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Facture } from 'src/app/entities/facture';
import { AppConfigService } from 'src/app/services/app-config.service';
import { PaiementService } from 'src/app/services/paiement.service';
import { Paiement } from 'src/app/entities/paiement';
import { SelectFactureDialogComponent } from 'src/app/dialogs/select-facture-dialog/select-facture-dialog.component';
import { Devise } from 'src/app/entities/devise';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';


@Component({
  selector: 'app-edit-paiement',
  templateUrl: './edit-paiement.component.html',
  styleUrls: ['./edit-paiement.component.css']
})
export class EditPaiementComponent implements OnInit {

  facture: Facture
  paiement: Paiement = new Paiement();
  devises: Devise[]

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: PaiementService,
    private configService: ConfigurationService
  ) { }

  ngOnInit(): void {
    this.reload();
  }

  openSelectFacture(): void{
    this.dialog.open(SelectFactureDialogComponent, {
      width: '90%',
      height: '90%',
    }).afterClosed().subscribe(
      (result: Facture) => {
        this.facture = result
        this.paiement.fkFacture = this.facture.numero;
        this.paiement.fkUser = this.appConfig.user.id;
        this.paiement.fkSite = this.appConfig.user.fkSite;
        this.paiement.fkDevise = this.facture.fkDefaultDevise;
        this.paiement.montantApayer = this.facture.montantTotal;
      }
    )
  }

  async reload(){
    await this.configService.getListeDevise().then(
      res => this.devises = res.response
    )
  }

  sumCDF(): number{
    return this.facture.montantTotal * this.facture.tauxDeChange
  }

  solde(): number{
    return Number.parseFloat((this.paiement.montantApayer - this.paiement.montantPaye).toFixed(3))
  }

  clear(): void{
    this.facture = null
        this.paiement.fkFacture = undefined
        this.paiement.fkDevise = undefined
        this.paiement.montantApayer = undefined
  }

  change(): void{
    if(this.paiement.fkDevise === 'CDF')
      this.paiement.montantApayer = this.sumCDF();
    else
    this.paiement.montantApayer = this.facture.montantTotal;
  }

  valider(): void{
    this.paiement.solde = this.solde()
    if(!this.paiement.montantPaye || this.paiement.montantPaye <=0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le montant payé');
      return;
    }
    if(this.paiement.solde > 0){
      Utility.openInfoDialog(this.dialog, 'Le montant saisi est incorrect');
      return;
    }

    Utility.openConfirmDialog(this.dialog, 'Êtes-vous sûr(e) de vouloir enregistement ce paiement?').afterClosed() 
    .subscribe(
      res => {
        if(res == 'OK'){
          this.service.effectuerUnPaiement(JSON.stringify(this.paiement))
          .then(
            result => {
              Utility.openSuccessDialog(this.dialog, result.error.errorDescription);
              this.clear();
            }
          ).catch(error => Utility.openInfoDialog(this.dialog, error))
        }
      }
    )
    
  }

}
