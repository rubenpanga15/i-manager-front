import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Devise } from 'src/app/entities/devise';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';
import { Fournisseur } from 'src/app/entities/fournisseur';

@Component({
  selector: 'app-edit-fournisseur',
  templateUrl: './edit-fournisseur.component.html',
  styleUrls: ['./edit-fournisseur.component.css']
})
export class EditFournisseurComponent implements OnInit {

  fournisseur: Fournisseur
  fournisseurs: Fournisseur[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    this.fournisseur = new Fournisseur();
    this.fournisseur.fkSite = this.user.fkSite;
  }

  filtreData(): Fournisseur[]{
    if(!this.fournisseurs) return []
    return this.fournisseurs.filter(e => e.noms.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  relaoad(): void{
    this.service.getListeFournisseurs()
    .then(
      result => this.fournisseurs = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: Fournisseur): void{
    this.fournisseur = item;
  }

  resetArticle(): void{
    this.fournisseur = undefined;
  }

  valider(): void{
    if(!this.fournisseur.noms || this.fournisseur.noms.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le nom');
      return;
    }

    if(!this.fournisseur.telephone || this.fournisseur.telephone.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le nom');
      return;
    }

    this.service.editFournisseur(JSON.stringify(this.fournisseur))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openErrorDialog(this.dialog, error))
  }

}
