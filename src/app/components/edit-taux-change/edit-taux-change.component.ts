import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Devise } from 'src/app/entities/devise';
import { User } from 'src/app/entities/user';
import { AppConfigService } from 'src/app/services/app-config.service';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Utility } from 'src/app/utilities/utility';
import { TauxDeChange } from 'src/app/entities/taux-de-change';

@Component({
  selector: 'app-edit-taux-change',
  templateUrl: './edit-taux-change.component.html',
  styleUrls: ['./edit-taux-change.component.css']
})
export class EditTauxChangeComponent implements OnInit {

  taux: TauxDeChange
  listeTaux: TauxDeChange[]
  devises: Devise[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    this.taux = new TauxDeChange();
    this.taux.fkSite = this.user.fkSite;
  }

  relaoad(): void{
    this.service.getListeTauxDeChange()
    .then(
      result => this.listeTaux = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: TauxDeChange): void{
    this.taux = item;
  }

  resetArticle(): void{
    this.taux = undefined;
  }

  valider(): void{
    if(!this.taux.fkDeviseFrom || this.taux.fkDeviseTo.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le taux de depart');
      return;
    }
    if(!this.taux.fkDeviseTo || this.taux.fkDeviseTo.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner le taux cible');
      return;
    }
    this.service.editTauxDeChange(JSON.stringify(this.taux))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openInfoDialog(this.dialog, error))
  }

}
