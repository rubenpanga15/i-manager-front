import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CategorieArticle } from '../../entities/categorie-article';
import { Site } from '../../entities/site';
import { User } from '../../entities/user';
import { AppConfigService } from '../../services/app-config.service';
import { ConfigurationService } from '../../services/configuration.service';
import { Utility } from '../../utilities/utility';

@Component({
  selector: 'app-edit-site',
  templateUrl: './edit-site.component.html',
  styleUrls: ['./edit-site.component.css']
})
export class EditSiteComponent implements OnInit {

  site: Site
  sites: Site[]

  user: User;

  motclef: string = ''

  constructor(
    private dialog: MatDialog,
    private appConfig: AppConfigService,
    private service: ConfigurationService
  ) {
    this.user = appConfig.user;
   }

  ngOnInit(): void {
    this.relaoad();
  }


  newArticle(): void{
    console.log(this.appConfig.user);
    this.site = new Site();
    this.site.description = ''
  }

  filtreData(): Site[]{
    if(!this.sites) return [];
    return this.sites.filter(e => e.description.toLowerCase().includes(this.motclef.toLowerCase()));
  }

  relaoad(): void{
    this.service.getListeSites()
    .then(
      result => this.sites = result.response
    ).catch(
      error => Utility.openInfoDialog(this.dialog, error)
    )
  }

  edit(item: CategorieArticle): void{
    this.site = item;
  }

  resetArticle(): void{
    this.site = undefined;
  }

  valider(): void{
    console.log(this.site)
    if(!this.site.description || this.site.description.length <= 0){
      Utility.openInfoDialog(this.dialog, 'Veuillez renseigner la designation');
      return;
    }
    this.service.editSite(JSON.stringify(this.site))
    .then(
      result => {
        Utility.openSuccessDialog(this.dialog,"Opération effectuée avec succès")
        this.resetArticle();
        this.relaoad();
      }
    ).catch(error => Utility.openErrorDialog(this.dialog, error))
  }

}
