import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-configuration',
  templateUrl: './main-configuration.component.html',
  styleUrls: ['./main-configuration.component.css']
})
export class MainConfigurationComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onMenuSelected(title: string): void{
    this.router.navigate(['main/' + title]);
  }

}
