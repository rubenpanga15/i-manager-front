import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MainComponent } from './components/main/main.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardSettingsComponent } from './components/dashboard-settings/dashboard-settings.component';
import { MainCommandeComponent } from './components/main-commande/main-commande.component';
import { MainGestionStockComponent } from './components/main-gestion-stock/main-gestion-stock.component';
import { MainPaiementComponent } from './components/main-paiement/main-paiement.component';
import { UserGuard } from './guards/user.guard';
import { MainConfigurationComponent } from './components/main-configuration/main-configuration.component';
import { EditCategorieArticlesComponent } from './components/edit-categorie-articles/edit-categorie-articles.component';
import { EditDeviseComponent } from './components/edit-devise/edit-devise.component';
import { EditSiteComponent } from './components/edit-site/edit-site.component';
import { EditFournisseurComponent } from './components/edit-fournisseur/edit-fournisseur.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { EditArticleComponent } from './components/edit-article/edit-article.component';
import { EditCommandeComponent } from './components/edit-commande/edit-commande.component';
import { EditTauxChangeComponent } from './components/edit-taux-change/edit-taux-change.component';
import { EditTaxeComponent } from './components/edit-taxe/edit-taxe.component';
import { EditConfigTarifComponent } from './components/edit-config-tarif/edit-config-tarif.component';
import { EditFactureComponent } from './components/edit-facture/edit-facture.component'
import { MainFactureComponent } from './components/main-facture/main-facture.component';
import { ListeFacturesComponent } from './components/liste-factures/liste-factures.component';
import { ListeCommandesComponent } from './components/liste-commandes/liste-commandes.component';
import { EditPaiementComponent } from './components/edit-paiement/edit-paiement.component';
import { ListeApurementComponent } from './components/liste-apurement/liste-apurement.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent
  },
  {
    path: 'main', component: MainComponent, canActivate: [UserGuard],
    children:[
      {
        path: '', component: DashboardComponent
      },
      {
        path: 'commande', component: MainCommandeComponent,
        children: [
          {
            path: 'edit-commande', component: EditCommandeComponent
          },
          {
            path: 'liste-commandes', component: ListeCommandesComponent
          },
        ]
      },
      {
        path: 'facture', component: MainFactureComponent,
        children: [
          {
            path: 'edit-facture', component: EditFactureComponent
          },
          {
            path: 'liste-factures', component: ListeFacturesComponent
          },
        ]
      },
      {
        path: 'gestion-stock', component: MainGestionStockComponent
      },
      {
        path: 'paiement', component: MainPaiementComponent,
        children: [
          {
            path: 'edit-paiement', component: EditPaiementComponent
          },
          {
            path: 'liste-paiement', component: ListeApurementComponent
          },
          
        ]
      },
      {
        path: 'settings', component: MainConfigurationComponent,
        children: [
          {
            path: '', component: DashboardSettingsComponent
          },
          {
            path: 'edit-categorie', component: EditCategorieArticlesComponent
          },
          {
            path: 'edit-devise', component: EditDeviseComponent
          },
          {
            path: 'edit-site', component: EditSiteComponent
          },
          {
            path: 'edit-fournisseur', component: EditFournisseurComponent
          },
          {
            path: 'edit-user', component: EditUserComponent
          },
          {
            path: 'edit-article', component: EditArticleComponent
          },
          {
            path: 'edit-taux', component: EditTauxChangeComponent
          },
          {
            path: 'edit-taxe', component: EditTaxeComponent
          },
          {
            path: 'edit-config', component: EditConfigTarifComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
